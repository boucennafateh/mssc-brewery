package org.fate7.msscbrewery.web.mapper;

import javax.annotation.Generated;
import org.fate7.msscbrewery.domain.Beer;
import org.fate7.msscbrewery.domain.Beer.BeerBuilder;
import org.fate7.msscbrewery.web.model.BeerDto;
import org.fate7.msscbrewery.web.model.BeerDto.BeerDtoBuilder;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-03-24T22:00:01+0100",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.1 (Oracle Corporation)"
)
public class BeerMapperImpl implements BeerMapper {

    private final DateMapper dateMapper = new DateMapper();

    @Override
    public BeerDto beerToBeerDto(Beer beer) {
        if ( beer == null ) {
            return null;
        }

        BeerDtoBuilder beerDto = BeerDto.builder();

        beerDto.id( beer.getId() );
        beerDto.name( beer.getName() );
        beerDto.style( beer.getStyle() );
        beerDto.upc( beer.getUpc() );
        beerDto.createdDate( dateMapper.toDateTime( beer.getCreatedDate() ) );
        beerDto.updatedDate( dateMapper.toDateTime( beer.getUpdatedDate() ) );

        return beerDto.build();
    }

    @Override
    public Beer beerDtoToBeer(BeerDto beerDto) {
        if ( beerDto == null ) {
            return null;
        }

        BeerBuilder beer = Beer.builder();

        beer.id( beerDto.getId() );
        beer.name( beerDto.getName() );
        beer.style( beerDto.getStyle() );
        beer.upc( beerDto.getUpc() );
        beer.createdDate( dateMapper.toTimestamp( beerDto.getCreatedDate() ) );
        beer.updatedDate( dateMapper.toTimestamp( beerDto.getUpdatedDate() ) );

        return beer.build();
    }
}
