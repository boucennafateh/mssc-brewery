package org.fate7.msscbrewery.web.mapper;

import javax.annotation.Generated;
import org.fate7.msscbrewery.domain.Customer;
import org.fate7.msscbrewery.domain.Customer.CustomerBuilder;
import org.fate7.msscbrewery.web.model.CustomerDto;
import org.fate7.msscbrewery.web.model.CustomerDto.CustomerDtoBuilder;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-03-24T22:00:02+0100",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.1 (Oracle Corporation)"
)
public class CustomerMapperImpl implements CustomerMapper {

    @Override
    public CustomerDto toCustomerDto(Customer customer) {
        if ( customer == null ) {
            return null;
        }

        CustomerDtoBuilder customerDto = CustomerDto.builder();

        customerDto.uuid( customer.getUuid() );
        customerDto.name( customer.getName() );

        return customerDto.build();
    }

    @Override
    public Customer toCustomer(CustomerDto customerDto) {
        if ( customerDto == null ) {
            return null;
        }

        CustomerBuilder customer = Customer.builder();

        customer.uuid( customerDto.getUuid() );
        customer.name( customerDto.getName() );

        return customer.build();
    }
}
