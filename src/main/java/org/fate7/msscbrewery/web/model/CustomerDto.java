package org.fate7.msscbrewery.web.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerDto {

    @Null
    private UUID uuid;

    @NotNull
    @NotBlank
    @Size(min = 3, max = 100, message = "the name should be between 3 and 100")
    private String name;
}
