package org.fate7.msscbrewery.web.service;

import org.fate7.msscbrewery.web.model.CustomerDto;

import java.util.UUID;

public interface CustomerService {

    CustomerDto getCustomerById(UUID id);
    CustomerDto saveNewCustomer(CustomerDto customerDto);
    void updateCustomer(UUID uuid,  CustomerDto customerDto);
    void deleteCustomer(UUID uuid);
}
