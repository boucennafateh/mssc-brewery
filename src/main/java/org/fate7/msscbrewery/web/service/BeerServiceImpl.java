package org.fate7.msscbrewery.web.service;

import lombok.extern.slf4j.Slf4j;
import org.fate7.msscbrewery.web.model.BeerDto;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class BeerServiceImpl implements BeerService {

    public BeerDto getBeerById(UUID id){
        return BeerDto.builder()
                .id(UUID.randomUUID())
                .name("Guinese")
                .style("Heavy")
                .upc(123l)
                .build();

    }

    @Override
    public BeerDto saveNewBeer(BeerDto beerDto) {
        beerDto.setId(UUID.randomUUID());
        return beerDto;
    }

    @Override
    public void updateBeer(UUID id, BeerDto beerDto) {
        //todo - impl
    }

    @Override
    public void deleteBeer(UUID id) {
        log.debug("Beer deleted : " + id);
    }


}
