package org.fate7.msscbrewery.web.service;

import org.fate7.msscbrewery.web.model.BeerDto;

import java.util.UUID;

public interface BeerService {

    BeerDto getBeerById(UUID id);
    BeerDto saveNewBeer(BeerDto beerDto);
    void updateBeer(UUID id, BeerDto beerDto);
    void deleteBeer(UUID id);

}
