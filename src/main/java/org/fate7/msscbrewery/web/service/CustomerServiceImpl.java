package org.fate7.msscbrewery.web.service;

import lombok.extern.slf4j.Slf4j;
import org.fate7.msscbrewery.web.model.CustomerDto;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class CustomerServiceImpl implements CustomerService{

    @Override
    public CustomerDto getCustomerById(UUID id) {
        return CustomerDto.builder()
                .uuid(id)
                .name("Christophe")
                .build();
    }

    @Override
    public CustomerDto saveNewCustomer(CustomerDto customerDto) {
        customerDto.setUuid(UUID.randomUUID());
        return customerDto;
    }

    @Override
    public void updateCustomer(UUID uuid, CustomerDto customerDto) {
        log.debug("Customer updated : " + uuid);

    }

    @Override
    public void deleteCustomer(UUID uuid) {
        log.debug("Customer deleted : " + uuid);

    }
}
