package org.fate7.msscbrewery.web.mapper;

import org.fate7.msscbrewery.domain.Beer;
import org.fate7.msscbrewery.web.model.BeerDto;
import org.mapstruct.Mapper;

@Mapper(uses = {DateMapper.class})
public interface BeerMapper {

    BeerDto beerToBeerDto(Beer beer);
    Beer beerDtoToBeer(BeerDto beerDto);

}
