package org.fate7.msscbrewery.web.mapper;

import org.fate7.msscbrewery.domain.Customer;
import org.fate7.msscbrewery.web.model.CustomerDto;
import org.mapstruct.Mapper;

@Mapper(uses = {DateMapper.class})
public interface CustomerMapper {
    CustomerDto toCustomerDto(Customer customer);
    Customer toCustomer(CustomerDto customerDto);
}
