package org.fate7.msscbrewery.web.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.test.context.ActiveProfiles;

@JsonTest
@ActiveProfiles("kebab")
public class BeerDtoKebabTest extends BaseTest{


    @Test
    public void testObjectSerialization() throws JsonProcessingException {
        BeerDto beerDto = getDto();
        String beerJson = objectMapper.writeValueAsString(beerDto);
        System.out.println(beerJson);
    }
}
