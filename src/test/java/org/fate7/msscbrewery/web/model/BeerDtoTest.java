package org.fate7.msscbrewery.web.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;

import static org.junit.jupiter.api.Assertions.*;

@JsonTest
class BeerDtoTest extends BaseTest {

    @Test
    public void testObjectSerialization() throws JsonProcessingException {
        BeerDto beerDto = getDto();
        String beerJson = objectMapper.writeValueAsString(beerDto);
        System.out.println(beerJson);
    }

    @Test
    public void testObjectDeserialization() throws JsonProcessingException {
        String beerJson = "{\"name\":\"beer\",\"style\":\"style\",\"upc\":123,\"price\":\"12.99\",\"createdDate\":\"2022-03-24T22:19:14+0100\",\"updatedDate\":\"2022-03-24T22:19:14.766262519+01:00\",\"myLocalDate\":\"20220324\",\"beerId\":\"62e7b14f-3cd1-4c20-8265-6c26b6eb00cc\"}\n";
        BeerDto beerDto = objectMapper.readValue(beerJson, BeerDto.class);
        System.out.println("beerDto = " + beerDto);
    }

}